# Usage

## Checkout

	git clone git@bitbucket.org:suddani/irrlicht-base.git
	cd irrlicht-base

## Setup build files

	mkdir build
	cd build
	cmake ../

## Install and build Irrlicht

	sudo apt-get install build-essential xserver-xorg-dev x11proto-xf86vidmode-dev libxxf86vm-dev mesa-common-dev libgl1-mesa-dev libglu1-mesa-dev libxext-dev libxcursor-dev
	make bootstrap

## Run the game

	make run

## After adding or removing source files

	cmake ../;make run